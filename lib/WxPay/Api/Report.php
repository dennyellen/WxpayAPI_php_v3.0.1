<?php

namespace lib\WxPay\Api;

use lib\WxPay\WxPayException;
use lib\WxPay\Config;
use lib\WxPay\Data\Report as reportData;

class Report
{
    /**
     *
     * 测速上报，该方法内部封装在report中，使用时请注意异常流程
     * WxPayReport中interface_url、return_code、result_code、user_ip、execute_time_必填
     * appid、mchid、spbill_create_ip、nonce_str不需要填入
     * @param reportData $inputObj
     * @param int $timeOut
     * @return mixed 成功时返回，其他抛异常
     * @throws WxPayException
     */
    public static function report(reportData $inputObj, $timeOut = 1)
    {
        $url = "https://api.mch.weixin.qq.com/payitil/report";
        //检测必填参数
        if (!$inputObj->IsInterface_urlSet()) {
            throw new WxPayException("接口URL，缺少必填参数interface_url！");
        } if (!$inputObj->IsReturn_codeSet()) {
        throw new WxPayException("返回状态码，缺少必填参数return_code！");
    } if (!$inputObj->IsResult_codeSet()) {
        throw new WxPayException("业务结果，缺少必填参数result_code！");
    } if (!$inputObj->IsUser_ipSet()) {
        throw new WxPayException("访问接口IP，缺少必填参数user_ip！");
    } if (!$inputObj->IsExecute_time_Set()) {
        throw new WxPayException("接口耗时，缺少必填参数execute_time_！");
    }
        $inputObj->SetAppid(Config::APPID);//公众账号ID
        $inputObj->SetMch_id(Config::MCHID);//商户号
        $inputObj->SetUser_ip($_SERVER['REMOTE_ADDR']);//终端ip
        $inputObj->SetTime(date("YmdHis"));//商户上报时间
        $inputObj->SetNonce_str(Helper::getNonceStr());//随机字符串

        $inputObj->SetSign();//签名
        $xml = $inputObj->ToXml();

        $startTimeStamp = Helper::getMillisecond();//请求开始时间
        $response = Helper::postXmlCurl($xml, $url, false, $timeOut);
        return $response;
    }

    /**
     *
     * 上报数据， 上报的时候将屏蔽所有异常流程
     * @param $url
     * @param int $startTimeStamp
     * @param array $data
     */
    public static function reportCostTime($url, $startTimeStamp, $data)
    {
        //如果不需要上报数据
        if (Config::REPORT_LEVENL == 0) {
            return;
        }
        //如果仅失败上报
        if (Config::REPORT_LEVENL == 1 &&
            array_key_exists("return_code", $data) &&
            $data["return_code"] == "SUCCESS" &&
            array_key_exists("result_code", $data) &&
            $data["result_code"] == "SUCCESS") {
            return;
        }

        //上报逻辑
        $endTimeStamp = Helper::getMillisecond();
        $objInput = new reportData();
        $objInput->SetInterface_url($url);
        $objInput->SetExecute_time_($endTimeStamp - $startTimeStamp);
        //返回状态码
        if (array_key_exists("return_code", $data)) {
            $objInput->SetReturn_code($data["return_code"]);
        }
        //返回信息
        if (array_key_exists("return_msg", $data)) {
            $objInput->SetReturn_msg($data["return_msg"]);
        }
        //业务结果
        if (array_key_exists("result_code", $data)) {
            $objInput->SetResult_code($data["result_code"]);
        }
        //错误代码
        if (array_key_exists("err_code", $data)) {
            $objInput->SetErr_code($data["err_code"]);
        }
        //错误代码描述
        if (array_key_exists("err_code_des", $data)) {
            $objInput->SetErr_code_des($data["err_code_des"]);
        }
        //商户订单号
        if (array_key_exists("out_trade_no", $data)) {
            $objInput->SetOut_trade_no($data["out_trade_no"]);
        }
        //设备号
        if (array_key_exists("device_info", $data)) {
            $objInput->SetDevice_info($data["device_info"]);
        }

        try {
            self::report($objInput);
        } catch (WxPayException $e) {
            //不做任何处理
        }
    }

}