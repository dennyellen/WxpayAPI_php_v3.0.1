<?php

namespace lib\WxPay\Api;

use lib\WxPay\Config;
use lib\WxPay\WxPayException;
use lib\WxPay\Data\CloseOrder;
use lib\WxPay\Data\UnifiedOrder;
use lib\WxPay\Data\OrderQuery;
use lib\WxPay\Data\Results;

class Order
{
    /**
     *
     * 统一下单，WxPayUnifiedOrder中out_trade_no、body、total_fee、trade_type必填
     * appid、mchid、spbill_create_ip、nonce_str不需要填入
     * @param UnifiedOrder $inputObj
     * @param int $timeOut
     * @return array
     * @throws WxPayException
     */
    public static function unified(UnifiedOrder $inputObj, $timeOut = 6)
    {
        $url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        //检测必填参数
        if (!$inputObj->IsOut_trade_noSet()) {
            throw new WxPayException("缺少统一支付接口必填参数out_trade_no！");
        } elseif (!$inputObj->IsBodySet()) {
            throw new WxPayException("缺少统一支付接口必填参数body！");
        } elseif (!$inputObj->IsTotal_feeSet()) {
            throw new WxPayException("缺少统一支付接口必填参数total_fee！");
        } elseif (!$inputObj->IsTrade_typeSet()) {
            throw new WxPayException("缺少统一支付接口必填参数trade_type！");
        }

        //关联参数
        if ($inputObj->GetTrade_type() == "JSAPI" && !$inputObj->IsOpenidSet()) {
            throw new WxPayException("统一支付接口中，缺少必填参数openid！trade_type为JSAPI时，openid为必填参数！");
        }
        if ($inputObj->GetTrade_type() == "NATIVE" && !$inputObj->IsProduct_idSet()) {
            throw new WxPayException("统一支付接口中，缺少必填参数product_id！trade_type为JSAPI时，product_id为必填参数！");
        }

        //异步通知url未设置，则使用配置文件中的url
        if (!$inputObj->IsNotify_urlSet()) {
            $inputObj->SetNotify_url(Config::NOTIFY_URL);//异步通知url
        }

        $inputObj->SetAppid(Config::APPID);//公众账号ID
        $inputObj->SetMch_id(Config::MCHID);//商户号
        $inputObj->SetSpbill_create_ip($_SERVER['REMOTE_ADDR']);//终端ip
        //$inputObj->SetSpbill_create_ip("1.1.1.1");
        $inputObj->SetNonce_str(Helper::getNonceStr());//随机字符串

        //签名
        $inputObj->SetSign();
        $xml = $inputObj->ToXml();

        $startTimeStamp = Helper::getMillisecond();//请求开始时间
        $response = Helper::postXmlCurl($xml, $url, false, $timeOut);
        $result = Results::Init($response);
        Report::reportCostTime($url, $startTimeStamp, $result);//上报请求花费时间

        return $result;
    }

    /**
     *
     * 查询订单，WxPayOrderQuery中out_trade_no、transaction_id至少填一个
     * appid、mchid、spbill_create_ip、nonce_str不需要填入
     * @param OrderQuery $inputObj
     * @param int $timeOut
     * @return array
     * @throws WxPayException
     */
    public static function Query(OrderQuery$inputObj, $timeOut = 6)
    {
        $url = "https://api.mch.weixin.qq.com/pay/orderquery";
        //检测必填参数
        if (!$inputObj->IsOut_trade_noSet() && !$inputObj->IsTransaction_idSet()) {
            throw new WxPayException("订单查询接口中，out_trade_no、transaction_id至少填一个！");
        }
        $inputObj->SetAppid(Config::APPID);//公众账号ID
        $inputObj->SetMch_id(Config::MCHID);//商户号
        $inputObj->SetNonce_str(Helper::getNonceStr());//随机字符串

        $inputObj->SetSign();//签名
        $xml = $inputObj->ToXml();

        $startTimeStamp = Helper::getMillisecond();//请求开始时间
        $response = Helper::postXmlCurl($xml, $url, false, $timeOut);
        $result = Results::Init($response);
        Report::reportCostTime($url, $startTimeStamp, $result);//上报请求花费时间

        return $result;
    }

    /**
     *
     * 关闭订单，WxPayCloseOrder中out_trade_no必填
     * appid、mchid、spbill_create_ip、nonce_str不需要填入
     * @param CloseOrder $inputObj
     * @param int $timeOut
     * @return array
     * @throws WxPayException
     */
    public static function close(CloseOrder $inputObj, $timeOut = 6)
    {
        $url = "https://api.mch.weixin.qq.com/pay/closeorder";
        //检测必填参数
        if (!$inputObj->IsOut_trade_noSet()) {
            throw new WxPayException("订单查询接口中，out_trade_no必填！");
        }
        $inputObj->SetAppid(Config::APPID);//公众账号ID
        $inputObj->SetMch_id(Config::MCHID);//商户号
        $inputObj->SetNonce_str(Helper::getNonceStr());//随机字符串

        $inputObj->SetSign();//签名
        $xml = $inputObj->ToXml();

        $startTimeStamp = Helper::getMillisecond();//请求开始时间
        $response = Helper::postXmlCurl($xml, $url, false, $timeOut);
        $result = Results::Init($response);
        Report::reportCostTime($url, $startTimeStamp, $result);//上报请求花费时间

        return $result;
    }
}
